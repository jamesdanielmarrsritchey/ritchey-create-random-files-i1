<?php
#Name:Random Ritchey Base Number v1
#Description:Create a random Ritchy Base Number. Returns the number as a string on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'size' (required) is a string containing a number specifying how many digits long the random number should be (it must be even). 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):size:number:required,display_errors:bool:optional
#Content:
if (function_exists('random_ritchey_base_number_v1') === FALSE){
function random_ritchey_base_number_v1($size, $display_errors = NULL){
	$errors = array();
	$progress = '';
	##Arguments
	if (@ctype_digit($size) === FALSE){
		$errors[] = "size";
	}
	if ($size % 2 == 0) {
		#Do nothing, because the number is even.
	} else {
		$errors[] = "size";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;	
	}
	if ($display_errors === TRUE OR $display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Create an array and add random digits to it 2 at a time until it reaches the required length.]
	if (@empty($errors) === TRUE){
		$number = array();
		$i = 0;
		while ($i < $size) {
			$number[] = @rand(10,74);
			$i++;
			$i++;
		}
		###Convert array to string.
		$number = @implode($number);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE and @empty($errors === FALSE)){
		$message = @implode(", ", $errors);
		if (function_exists('random_ritchey_base_number_v1_format_error') === FALSE){
			function random_ritchey_base_number_v1_format_error($errno, $errstr){
				echo $errstr;
			}
		}
		set_error_handler("random_ritchey_base_number_v1_format_error");
		trigger_error($message, E_USER_ERROR);
	}
	##Return
	if (@empty($errors) === TRUE){
		return $number;
	} else {
		return FALSE;
	}
}
}
?>