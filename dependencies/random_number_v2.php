<?php
#Name:Random Number v2
#Description:Create a random number. Returns the number as a string on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'size' (required) is a string containing a number specifying how many digits long the random number should be. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):size:number:required,display_errors:bool:optional
#Content:
if (function_exists('random_number_v2') === FALSE){
function random_number_v2($size, $display_errors = NULL){
	$errors = array();
	$progress = '';
	##Arguments
	if (@ctype_digit($size) === FALSE){
		$errors[] = "size";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;	
	}
	if ($display_errors === TRUE OR $display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Create an array and add random digits to it 1 at a time until it reaches the required length.]
	if (@empty($errors) === TRUE){
		$number = array();
		$i = 0;
		while ($i < $size) {
			$number[] = @rand(0,9);
			$i++;
		}
		###Ensure the first digit isn't a zero. If it is change it to a random number between 1 and 9.
		if ($number[0] == '0'){
			$number[0] = @rand(1,9);		
		}
		###Convert array back to string.
		$number = @implode($number);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE and @empty($errors === FALSE)){
		$message = @implode(", ", $errors);
		if (function_exists('random_number_v2_format_error') === FALSE){
			function random_number_v2_format_error($errno, $errstr){
				echo $errstr;
			}
		}
		set_error_handler("random_number_v2_format_error");
		trigger_error($message, E_USER_ERROR);
	}
	##Return
	if (@empty($errors) === TRUE){
		return $number;
	} else {
		return FALSE;
	}
}
}
?>